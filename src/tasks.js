export default [
	{
        'id': '1',
        'name': 'String theory',
		'description': 'prove the string theory',
		'date': '2016-06-09T15:03:23.000Z',
        'plannedTime': '24',
        'spentTime': '20',
		'status': 'procesing'
	},
	{
        'id': '2',
		'name': 'Artificial Intelligence',
		'description': 'create a machine with a consciousness',
		'date': '2016-06-09T15:03:23.000Z',
        'plannedTime': '30',
        'spentTime': '10',
		'status': 'planing'
	},
	{
        'id': '3',
		'name': 'The Last Frontier',
		'description': 'create a colony on Mars',
		'date': '2016-06-09T15:03:23.000Z',
        'plannedTime': '15',
        'spentTime': '13',
		'status': 'done'
	}
]
