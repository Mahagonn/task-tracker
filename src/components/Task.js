import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Task extends Component {
  state = {
    fullView: this.props.viewTaskList
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.viewTaskList !== this.props.viewTaskList) {
      this.setState({fullView: nextProps.viewTaskList});
    }
  }

  onClick = () => {
    this.setState({
      fullView: !this.state.fullView
    });
  }

  render () {
    const {task} = this.props;
    const body = this.state.fullView && <section>
      description: {task.description} <br />
      planned time: {task.plannedTime} hours <br />
      spent time: {task.spentTime} hours  <br />
      status: {task.status}	<br />
      date: {new Date(task.date).toDateString()} <br />
    </section>;

    return (
      <div>
          <h2> 
            {task.name}
            <button onClick = {this.onClick}>
              {this.state.fullView ? 'Minimize' : 'Expand'}
            </button>
          </h2> 
          {body}
      </div>
    );
  }
}

Task.propTypes = {
  task: PropTypes.object.isRequired,
  viewTaskList: PropTypes.bool.isRequired
};