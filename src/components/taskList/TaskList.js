import React, {Component} from 'react'
import Task from '../Task'
import './TaskList.css';
import PropTypes from 'prop-types';

export default class TaskList extends Component {
  buildTaskList = () => {
    const viewTaskList = this.props.viewTaskList;
    const taskList = this.props.tasks.map(task =>
      <li key = {task.id} className = "task-list__li">
        <Task task = {task} viewTaskList = {viewTaskList}/> 
      </li>
    )
    return taskList;
  }

  render() {
    const taskList = this.buildTaskList();
    return (
      <div>
        <ul>
          {taskList}
        </ul>
      </div>
    );
  }
}

TaskList.propTypes = {
  tasks: PropTypes.array.isRequired,
  viewTaskList: PropTypes.bool.isRequired
};