import React, { Component } from 'react';
import users from '../users';
import PropTypes from 'prop-types';


export default class Login extends Component{
  state = {
    login: '',
    password: '',
    logged: false
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  checkLogin = () => {
    let isUser = users.filter(user => {
      return user.login === this.state.login && user.password === this.state.password
    });
    if (isUser.length === 1) {
      this.setState({
        login: '',
        password: '',
      });
      this.props.open(isUser[0].login)
    }
  }

  onSubmit = e => {
    e.preventDefault();
    this.checkLogin();
  }

  render() {
    return(
    <form> 
      <h1>Log in</h1>
      <input 
        name = 'login'
        placeholder = 'login'
        value = {this.state.login} 
        onChange = {e => this.onChange(e)}
      /> 
      <br />
      <input 
        name = 'password'
        type = 'password'
        placeholder = 'password'
        value = {this.state.password} 
        onChange = {e => this.onChange(e)}
      />
      <br />
      <button onClick = {e => this.onSubmit(e)}>Submit</button>
    </form>
   );
 }
}
Login.propTypes = {
  open: PropTypes.func.isRequired
};