import React, { Component } from 'react';
import Login from './Login';
//import TaskList from './taskList/TaskList';
import tasks from '../tasks';
import '../App.css';
import Table from './table/Table';
import users from '../users';


export default class App extends Component {
  state = {
    viewTaskList: false,
    logged: true,
    user: 'admin'
  }
  updating = () => {
    let promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('update!');
      }, 60000);
    });
    promise
      .then(
        result => {
          this.filterUsersTasks();
          this.updating();
        },
        error => {
          alert(error);
        }
      );
  }
  filterUsersTasks = () => {
    if (this.state.user !== '') {
      console.log('update!');
      const indexes = users.filter(user => user.login === this.state.user)[0].tasks;
      const usersTasks = tasks.filter(arr => indexes.indexOf(+arr.id) !== -1);
      return usersTasks;
    } else {
      return null;
    }
  }

  loggingIn = (user) => {
    this.setState({ 
      user: user
      });
    this.setState({logged: true });
    this.updating();
  } 

  onClick = () => {
    this.setState({viewTaskList: !this.state.viewTaskList});
  }


  renderLogin = () => {
    const usersTasks = this.filterUsersTasks();
    if (this.state.logged) {
      return <section>
        <h1 className = "table-header">Tasks</h1>
        <Table data = {usersTasks}/>
      </section>;
    }
    return <section><Login open={this.loggingIn}/></section>;
  }
  //<h2>Task List:</h2>
  //      <h3>
  //        <button onClick = {this.onClick}>
  //          {this.state.viewTaskList ? 'Minimize' : 'Expand'}
  //        </button>
  //      </h3>
  //      <TaskList tasks = {usersTasks} viewTaskList = {this.state.viewTaskList}/>
  
  render() {
    const render = this.renderLogin();
    return (
      <div className = "app">
      {render}
      </div>
    );
  }
}

