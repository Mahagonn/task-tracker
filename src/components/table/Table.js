import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import './Table.css';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import PropTypes from 'prop-types';
 


export default class Table extends Component {
  state = {
    indexOfFilter: 0,
    sortedByName: false,
    data: []
  }

  //componentWillReceiveProps(nextProps) {
  //  if (nextProps.viewTaskList !== this.props.viewTaskList) {
  //    this.setState({fullView: nextProps.viewTaskList});
  // }
  //}

  componentDidMount() {
    setTimeout(() => {
      this.getData();
    }, 2000);
  }
  getData = () => {
    const data = this.props.data;
    this.setState({data: data});
    return data;
  }
  setButtonFilterName = () => {
    let filterName = ['none','planing','procesing','done'];
    return filterName[this.state.indexOfFilter]
  }
  filterData = () => {
    let filterBy = this.setButtonFilterName();
    let filter = this.state.data.filter(task => {
        if (filterBy==='none') {
          return true
        }
        return task.status === filterBy;
      }
    );
    filter = this.sortData(filter);
    return filter;
  }

  sortData = (filter) => {
    if (this.state.sortedByName === true) {
      filter.sort((one, two) => {
        if (one.name < two.name) return -1;
        if (one.name > two.name) return 1;
        return 0;
      }); 
    } else {
      filter.sort((one, two) => {
        if (one.id < two.id) return -1;
        if (one.id > two.id) return 1;
        return 0;
      }); 
    }
    return filter;
  }

  handleFilter = () => {
    if (+this.state.indexOfFilter === 3) {
      return this.setState({indexOfFilter: this.state.indexOfFilter-3});
    }
    this.setState({indexOfFilter: this.state.indexOfFilter+1});
  }
  
  handleSort = () => {
    return this.setState({sortedByName: !this.state.sortedByName});
  }

  render() {
    const filter = this.filterData();
    const data = this.state.data;
    function isExpandableRow(row) {
      return true
    }
     
    function expandRow(row) {
      return (
        <div>
        <p>description: {data.map(a => a.description)[row['id']-1]}</p>
        <p>plannedTime: {data.map(a => a.plannedTime)[row['id']-1]}</p>
        <p>spentTime: {data.map(a => a.spentTime)[row['id']-1]}</p>
        <p>date: {data.map(a => a.date)[row['id']-1]}</p>
        </div>
      );
    }

    return (
      <div>
        <button onClick = {this.handleFilter}>
          Filter: {this.setButtonFilterName(0)} 
        </button>
        <button onClick = {this.handleSort}>
          Sort by name 
        </button>
        <BootstrapTable data = {filter}
                                expandableRow = {isExpandableRow}
                                expandComponent = {expandRow}
        >
          <TableHeaderColumn isKey dataField = 'id'
                              width = "30"
          >
            ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField = 'name'
                              width = "200"
          >
            Name
          </TableHeaderColumn>
          <TableHeaderColumn dataField = 'status'
                             width = "100"
          >
            Status
          </TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

Table.propTypes = {
  data: PropTypes.array.isRequired
};
BootstrapTable.propTypes = {
  data: PropTypes.array.isRequired
};